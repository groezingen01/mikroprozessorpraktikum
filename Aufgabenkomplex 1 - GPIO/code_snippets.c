//A01-01.3
void init_leds(void) {
	// Declare GPIO Configuration Struct
	GPIO_InitTypeDef GPIO_InitStructure;
	//Activate peripheral clock for GPIO B
	//GPIO B is connected to the green LED
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	// Initialize GPIO Configuration
	GPIO_StructInit(&GPIO_InitStructure);
	// Set output to Pin 2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	// Set speed(polling rate)
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	// Set GPIO mode to output
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	// Select push-pull with no pull
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	// Assign the defined configuration to GPIO port B
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	// Initialize LED defaulte state(OFF)
	GR_LED_OFF;
}
//A01-01.4
//makros in aufgabe.h
#define green_LED_OFF (GPIO_ResetBits(GPIOB, GPIO_Pin_2))
#define green_LED_ON (GPIO_SetBits(GPIOB, GPIO_Pin_2))
#define green_LED_Toggle (GPIO_ToggleBits(GPIOB, GPIO_Pin_2))
//Blinken
int main(void {
//... Board Initialization
	init_leds();
	while (1) {
		wait_uSek(1000000);//u=10^-6 -> 10^6 us= 1s
		// Macro defined in aufgabe.h
		GR_LED_TOGGLE;
	}
}
//A01-02.2
void init_taste_1(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	// Activate peripheral clock for GPIO port C
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	// initialize GPIO with the defined struct
	GPIO_StructInit(&GPIO_InitStructure);
	// Set GPIO input to Pin 8
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	//Assign the defined configuration to GPIO port C
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}
void init_taste_2(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	// Activate peripheral clock for GPIO port C
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	// initialize GPIO with the defined struct
	GPIO_StructInit(&GPIO_InitStructure);
	// Set GPIO input to Pin 5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}
//A01-02.3

void manage_leds() {
	// intialize buttons :
	// default value for button1 is 1(PC8)
	init_taste_1();
	// default value for button2 is 0(PC5)
	init_taste_2();
	// Count how many times the button was pressed
	int btn2_counter = 0;
	uint8_t btn2_prev_value = 0;
	uint8_t btn1_value = 0;
	uint8_t btn2_value = 0;
	while (1) {
		btn2_prev_value = btn2_value;
		// Read input from GPIO Port C Pin 8 and store it
		btn1_value = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8);
		// Read input from GPIO Port C Pin 5 and store it
		btn2_value = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5);
		if (!(btn1_value)) {
			//Button 1 was pressed
			GR_LED_OFF;
		}
		if (btn2_value && !btn2_prev_value) {
			//Button2 is pressed and was not before
			btn2_counter++;
		}
		if (btn2_counter >= 2) {
			//Button2 was pressed exactly 2 times
			GR_LED_ON;
			// reset counter
			btn2_counter = 0;
		}
	}
}