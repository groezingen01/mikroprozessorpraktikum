
//A05-01.1

//in aufgabe.c definiert, in der main vor der Schleife aufgerufen
void init_taste_1_irq() {
	//==GPIO configuration
	init_taste_1();//From A02
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//Additional Clock
	//==Interrupt Configuration
	//Selects the GPIO pin used as EXTI Line and sets it to PC08
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);
	//Struct to configure externel interrupt
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line8;//LineX = PinX
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	//When the interrupt is supposed to be triggered.
	//Here on signal change from low to high.
	
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;//Allow Interrupt
	EXTI_Init(&EXTI_InitStructure);//write config to register
	//==Interruptcontroller/NCIC Configuration
	// Struct to configure interrupt controller
	NVIC_InitTypeDef NVIC_InitStructure;
	//One channel for all Pins between 5 and 9, including our pin 8
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);//Setting Priority to low priority
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	//Additional priority settings
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStructure);//write config to register
}
void init_taste_2_irq() {
	//==GPIO configuration
	init_taste_2();//From A02
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//Additional Clock
	//==Interrupt Configuration
	//Selects the GPIO pin used as EXTI Line and sets it to PC05
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource5);
	//Struct to configure externel interrupt
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line5;//LineX = PinX
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	//When the interrupt is supposed to be triggered.
	//Here on signal change from high to low
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;//Allow Interrupt
	EXTI_Init(&EXTI_InitStructure);//write config to register
	//==Interruptcontroller/NCIC Configuration
	// Struct to configure interrupt controller
	NVIC_InitTypeDef NVIC_InitStructure;
	//One channel for all Pins between 5 and 9
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);//Setting Priority to low priority
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	//Additional priority settings
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStructure);//write config to register
}

//A05-01.2
void init_taste_1_irq() {
	//==GPIO configuration
	init_taste_1();//From A02
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//Additional Clock
	//==Interrupt Configuration
	//Selects the GPIO pin used as EXTI Line and sets it to PC08
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);
	//Struct to configure externel interrupt
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line8;//LineX = PinX
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	//When the interrupt is supposed to be triggered.
	//Here on signal change from low to high.
	
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;//Allow Interrupt
	EXTI_Init(&EXTI_InitStructure);//write config to register
	//==Interruptcontroller/NCIC Configuration
	// Struct to configure interrupt controller
	NVIC_InitTypeDef NVIC_InitStructure;
	//One channel for all Pins between 5 and 9, including our pin 8
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);//Setting Priority to low priority
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	//Additional priority settings
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStructure);//write config to register
}
void init_taste_2_irq() {
	//==GPIO configuration
	init_taste_2();//From A02
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);//Additional Clock
	//==Interrupt Configuration
	//Selects the GPIO pin used as EXTI Line and sets it to PC05
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource5);
	//Struct to configure externel interrupt
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line5;//LineX = PinX
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	//When the interrupt is supposed to be triggered.
	//Here on signal change from high to low
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;//Allow Interrupt
	EXTI_Init(&EXTI_InitStructure);//write config to register
	//==Interruptcontroller/NCIC Configuration
	// Struct to configure interrupt controller
	NVIC_InitTypeDef NVIC_InitStructure;
	//One channel for all Pins between 5 and 9
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);//Setting Priority to low priority
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	//Additional priority settings
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStructure);//write config to register
}

//a05-01.3

//in interrupts.c
static unsigned int t2_ctr=0;
static unsigned int t1_ctr=0;
static unsigned int true_t2_ctr=0;
static unsigned int true_t1_ctr=0;
void taster1_irq(){
	t1_ctr++;
	true_t1_ctr++;
	char num_buff[50];
	usart_2_print("Taste1 gedrückt: ");//Sends String via USART, defined in A03
	sprintf(num_buff,"Taste 1 %d mal gedrückt. ",true_t1_ctr);
	usart_2_print(num_buff);
	sprintf(num_buff,"Taste 2 %d mal gedrückt.",true_t2_ctr);
	usart_2_print(num_buff);
		if (t1_ctr>=10){
		// Disable Interrupt for PC08
		EXTI_InitTypeDef EXTI_InitStructure;
		EXTI_InitStructure.EXTI_Line = EXTI_Line8;

		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
		EXTI_InitStructure.EXTI_LineCmd = DISABLE;
		EXTI_Init(&EXTI_InitStructure);
		t2_ctr=0;
		}
	green_LED_ON;
}
void taster2_irq(){
	t2_ctr++;
	true_t2_ctr++;
	char num_buff[50];
	usart_2_print("Taste2 gedrückt: ");
	sprintf(num_buff,"Taste 1 %d mal gedrückt. ",true_t1_ctr);
	usart_2_print(num_buff);
	sprintf(num_buff,"Taste 2 %d mal gedrückt.",true_t2_ctr);
	usart_2_print(num_buff);
	if (t2_ctr>=2){
		if (t1_ctr >=10){
		// Enable Interrupt for PC08
		EXTI_InitTypeDef EXTI_InitStructure;
		EXTI_InitStructure.EXTI_Line = EXTI_Line8;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);
		t2_ctr = 0;
		t1_ctr = 0;
		}
	}
	green_LED_OFF;
}

void EXTI9_5_IRQHandler(void)
{
	//===== Taster2
	if (EXTI_GetITStatus(EXTI_Line5) == SET)
		{
			EXTI_ClearFlag(EXTI_Line5);
			EXTI_ClearITPendingBit(EXTI_Line5);
			taster2_irq();
		}
	//===== nicht belegt
	if (EXTI_GetITStatus(EXTI_Line6) == SET)
		{
			EXTI_ClearFlag(EXTI_Line6);
			EXTI_ClearITPendingBit(EXTI_Line6);
			// nicht belegt
		}
	//===== nicht belegt
	if (EXTI_GetITStatus(EXTI_Line7) == SET)
		{
			EXTI_ClearFlag(EXTI_Line7);
			EXTI_ClearITPendingBit(EXTI_Line7);
			// nicht belegt
		}
	//===== Taster 1
	if (EXTI_GetITStatus(EXTI_Line8) == SET)
		{
			EXTI_ClearFlag(EXTI_Line8);
			EXTI_ClearITPendingBit(EXTI_Line8);
			taster1_irq();
		}
	//===== nicht belegt
	if (EXTI_GetITStatus(EXTI_Line9) == SET)
		{
			EXTI_ClearFlag(EXTI_Line9);
			EXTI_ClearITPendingBit(EXTI_Line9);
			// nicht belegt
		}
}
//A05-02.1
//in aufgabe.c aufgerufen in der main vor schleife
void init_usart_2_irq_rx() {
	init_usart_2();
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Die Freigabe des zugehörigen Interrupts sieht wie fogt aus:
	USART_ClearITPendingBit(USART2, USART_IT_RXNE);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}

//in aufgabe.c aufgerufen in der main vor schleife
void blink_handler(){
	while (led_enabled){
		if (led_timer >0){
		wait_mSek(led_timer*1000);
		green_LED_Toggle;
		}
	}
}

//in interrupts.c
void USART2_IRQHandler(void)
{
	char zeichen;
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		zeichen = (char)USART_ReceiveData(USART2);
		switch(zeichen){
			case '1':led_timer=1;break;
			case '4':led_timer=4;break;
			case 's':led_enabled=0;break;
			default:;
		}
	}
}
int led_enabled = 1;
int led_timer = -1;

//A05-02.2
//in interrupts.c
void USART2_IRQHandler(void)
{
    char zeichen;
    zeichen = (char)USART_ReceiveData(USART2);
    if (zeichen == '\r'){
        sprintf(tx_buffer,"\"%s\" hat %d Zeichen.",rx_buffer,pos_in_rx_buf);
        usart_2_print(tx_buffer);
        pos_in_rx_buf=0;
        memset(rx_buffer,0,128);
    }
    else{
        rx_buffer[pos_in_rx_buf]=zeichen;
        pos_in_rx_buf++;
    }
}