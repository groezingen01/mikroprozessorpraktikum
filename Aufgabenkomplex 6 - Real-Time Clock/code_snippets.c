//A06-01.1
//eingefügt in main
RTC_TimeTypeDef  RTC_Time_Aktuell;      // stores time
RTC_DateTypeDef  RTC_Date_Aktuell;      // stores date
while (1)
{
    //used to output day of the week as string instead of int
    char date_arr[7][20] =  {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"
    };
    char data[50] = {0};

    
    RTC_GetDate(RTC_Format_BIN, &RTC_Date_Aktuell);// load date into struct
    //formating and sending output
    sprintf(data,"%.2d-%.2d-%.2d-%s",RTC_Date_Aktuell.RTC_Year,RTC_Date_Aktuell.RTC_Month, RTC_Date_Aktuell.RTC_Date,date_arr[--RTC_Date_Aktuell.RTC_WeekDay]);
    usart_2_print(data);

    
    RTC_GetTime(RTC_Format_BIN, &RTC_Time_Aktuell);// load time into struct
    //formating and sending output
    sprintf(data,"%.2d:%.2d:%.2d:%.2d",RTC_Time_Aktuell.RTC_Hours,RTC_Time_Aktuell.RTC_Minutes, RTC_Time_Aktuell.RTC_Seconds,RTC_Time_Aktuell.RTC_H12);
    usart_2_print(data);
    wait_uSek(1000000);//wait 1 sec
}

//A06-01.2
//in interrupts.c
void USART2_IRQHandler(void)
{

    char zeichen;
    zeichen = (char)USART_ReceiveData(USART2);
    if (zeichen == '\r'){
        sprintf(tx_buffer,"\"Input: %s\"",rx_buffer);
        usart_2_print(tx_buffer);
        interp_input(rx_buffer);//helper function, interprets the given input
        pos_in_rx_buf=0;
        memset(rx_buffer,0,128);
    }
    else{
        rx_buffer[pos_in_rx_buf]=zeichen;
        pos_in_rx_buf++;
        if (pos_in_rx_buf>127){//preventing overflow of rx buffer
            pos_in_rx_buf=0;
            memset(rx_buffer,0,128);
            usart_2_print("Input to long. Had to flush buffer.");
        }
    }
}

void interp_input(char* str){
    RTC_TimeTypeDef RTC_Time_Struct;
    RTC_DateTypeDef RTC_Date_Struct;

    if(str[2]=='-'){//date syntax
        //try to set date
        if (strlen(str)!= 11){
            usart_2_print("Wrong input.");
            return;
        }
        int YY , MM , DD, WD;
        if (sscanf(str , "%2d-%2d-%2d-%2d", &YY , &MM , &DD,&WD)==4){
            if (YY <=99 && MM <= 12 && DD <= 31 && WD <=7){//Chek if input is a valid date
                usart_2_print("Set date: ");
                usart_2_print(str);
                RTC_Date_Struct.RTC_Date=DD;
                RTC_Date_Struct.RTC_Month=MM;
                RTC_Date_Struct.RTC_Year=YY;
                RTC_Date_Struct.RTC_WeekDay=WD;
                if (hh <=12){
                        RTC_Time_Struct.RTC_H12=RTC_H12_AM;
                    }
                    else {
                        RTC_Time_Struct.RTC_H12=RTC_H12_PM;
                    }

                //Use struct to set current date
                RTC_SetDate(RTC_Format_BIN, &RTC_Date_Struct);
            }
            else{
                usart_2_print("Not a valid date.");
            }

        }
    }
    else {
        if (str[2]==':'){//time syntax
            //try to set time
            if (strlen(str)!= 8){
                        usart_2_print("Wrong input.");
                        return;
            }
            int hh , mm , ss;
            if (sscanf(str , "%2d:%2d:%2d", &hh , &mm , &ss)==3){
                if (hh <= 23 && mm <= 59 && ss <=59){//Check if input is a valid time
                    usart_2_print("Set time:");
                    usart_2_print(str);
                    RTC_Time_Struct.RTC_Hours=hh;
                    RTC_Time_Struct.RTC_Minutes=mm;
                    RTC_Time_Struct.RTC_Seconds=ss;
                    //use struct to set current time
                    RTC_SetTime(RTC_Format_BIN, &RTC_Time_Struct);
                }
                else{
                    usart_2_print("Not a valid time.");
                }

            }
        }else{
            //Neither time nor date
            usart_2_print("Wrong input.");
        }
    }
}

//A06-02.1
//in aufgabe.c aufgerufen vor Schleife in main
void init_alarm_a1(){
    //==Konfigurieren des Alarms zu jeder 30igsten Sekunde
    RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

    RTC_AlarmTypeDef RTC_Alarm_Struct;

    // Alarmzeit setzen..
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_H12      = RTC_H12_AM;
    //Strunde und Minute sind egal, da dies eh wegmaskiert wird
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours    = 1;
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes  = 1;
    
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds  = 30;

    // Alarmmaske setzen: 
    //Der Alarm soll zu jeder 30igsten Sekunde passieren,
    //unabhängig von Tag, Stunde oder Minute
    RTC_Alarm_Struct.RTC_AlarmMask          = RTC_AlarmMask_DateWeekDay | 
        RTC_AlarmMask_Hours |  RTC_AlarmMask_Minutes;

    //Ob der Alarm sich nach Wochentag oder Datum richtet
    //Ebenfalls egal
    RTC_Alarm_Struct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;

    // Alarm Tag oder Wochentag setzen, da maskiert egal
    RTC_Alarm_Struct.RTC_AlarmDateWeekDay    = 0;  

    // Setzen der Konfiguration von Alarm A
    RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_Alarm_Struct);


    //==Konfigurieren des Interrrupts
    // Anlegen der benötigten Structs
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    //Die EXTI-Line des Alarm wird nun aktiviert
    EXTI_ClearITPendingBit(EXTI_Line17);
    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    // NIVC Konfiguration
    NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    // Konfigurieren des Alarm A
    RTC_ITConfig(RTC_IT_ALRA, ENABLE);

    // RTC Alarm A freigeben
    RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

    // Alarmflag löschen
    RTC_ClearFlag(RTC_FLAG_ALRAF);
    // Die zutreffende ISR muß auch bereitgestellt worden sein
}

//in aufgabe.c aufgerufen vor Schleife in main
void init_alarm_a2(){
    //==Konfigurieren des Alarms zu jeder 30igsten Sekunde
    RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

    RTC_AlarmTypeDef RTC_Alarm_Struct;

    // Alarmzeit setzen..
   //Alarmzeit setzen
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_H12      = RTC_H12_AM;
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours    = 0;
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes  = 30;
    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds  = 00;

    //Bei diesen Alarm muss nichts maskiert werden
    RTC_Alarm_Struct.RTC_AlarmMask = RTC_AlarmMask_None;

    // Alarm auf Basis des Wochentags, nicht des Datums
    RTC_Alarm_Struct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_WeekDay;

    //Wochentag auf Montag setzten
    RTC_Alarm_Struct.RTC_AlarmDateWeekDay    = RTC_Weekday_Monday; 

    //Setzen der Alarm-Konfiguration
    RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_Alarm_Struct);


    //==Konfigurieren des Interrrupts
    // Anlegen der benötigten Structs
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    //Die EXTI-Line des Alarm wird nun aktiviert
    EXTI_ClearITPendingBit(EXTI_Line17);
    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    // NIVC Konfiguration
    NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    // Konfigurieren des Alarm A
    RTC_ITConfig(RTC_IT_ALRA, ENABLE);

    // RTC Alarm A freigeben
    RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

    // Alarmflag löschen
    RTC_ClearFlag(RTC_FLAG_ALRAF);
    // Die zutreffende ISR muß auch bereitgestellt worden sein
}

//interrupts.c, erster Alarm
void RTC_Alarm_IRQHandler(void){
    //===== RTC_IT_ALRA: Alarm A interrupt
    if(RTC_GetITStatus(RTC_IT_ALRA) != RESET)
    {
        RTC_ClearITPendingBit(RTC_IT_ALRA);
        EXTI_ClearITPendingBit(EXTI_Line17);
        usart_2_print("ALARM,ALARM!");
    }
}

//interrupts.c, zweiter Alarm
void RTC_Alarm_IRQHandler(void){
    //===== RTC_IT_ALRA: Alarm A interrupt
    if(RTC_GetITStatus(RTC_IT_ALRA) != RESET)
    {
        RTC_ClearITPendingBit(RTC_IT_ALRA);
        EXTI_ClearITPendingBit(EXTI_Line17);
        usart_2_print("ALARM,ALARM! CURRENT TIME:");
        print_time();
    }
}

//in aufgabe.c
void print_time(){
    RTC_TimeTypeDef  RTC_Time_Aktuell;      //  Zeit
    // Zeit aus der RTC in das Struct laden
	RTC_GetTime(RTC_Format_BIN, &RTC_Time_Aktuell);
	char data[50] = {0};
	sprintf(data,"%.2d:%.2d:%.2d:%.2d",RTC_Time_Aktuell.RTC_Hours, RTC_Time_Aktuell.RTC_Minutes, RTC_Time_Aktuell.RTC_Seconds, RTC_Time_Aktuell.RTC_H12);
	usart_2_print(data);
}

//A06-02.2
//in aufgabe.c, in main vor schleife aufgerufen
void reset_alarm_25s(){
		//==Konfigurieren des Alarms auf 25s in der Zukunft
	    RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

	    RTC_AlarmTypeDef RTC_Alarm_Struct;
	    RTC_TimeTypeDef  RTC_Time_Aktuell;

	    // Zeit aus der RTC in das Struct laden
	    RTC_GetTime(RTC_Format_BIN, &RTC_Time_Aktuell);

	    // Alarmzeit setzen..
	    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours    = RTC_Time_Aktuell.RTC_Hours;
	    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes  = RTC_Time_Aktuell.RTC_Minutes;

	    int time = RTC_Time_Aktuell.RTC_Seconds +25;
	    if (time> 59){
	    	time = time-60;
	    }
	    RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds  = time;

	    RTC_Alarm_Struct.RTC_AlarmMask =  RTC_AlarmMask_DateWeekDay // Wochentag oder Tag ausgeblendet
	                                    | RTC_AlarmMask_Hours       // Stunde ausgeblendet
	                                    | RTC_AlarmMask_Minutes;    // Minute ausgeblendet

	    // Alarm für den Tag oder Wochentag auswählen
	    RTC_Alarm_Struct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date; // Tag (1-31)
	    // Alarm Tag oder Wochentag setzen
	    RTC_Alarm_Struct.RTC_AlarmDateWeekDay    = 0x01;

	    // Setzen der Konfiguration von Alarm A
	    RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_Alarm_Struct);


	    //==Konfigurieren des Interrrupts
	    // Anlegen der benötigten Structs
	    EXTI_InitTypeDef EXTI_InitStructure;
	    NVIC_InitTypeDef NVIC_InitStructure;

	    //Die EXTI-Line des Alarm wird nun aktiviert
	    EXTI_ClearITPendingBit(EXTI_Line17);
	    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	    EXTI_Init(&EXTI_InitStructure);

	    // NIVC Konfiguration
	    NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	    NVIC_Init(&NVIC_InitStructure);

	    // Konfigurieren des Alarm A
	    RTC_ITConfig(RTC_IT_ALRA, ENABLE);

	    // RTC Alarm A freigeben
	    RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

	    // Alarmflag löschen
	    RTC_ClearFlag(RTC_FLAG_ALRAF);
	    // Die zutreffende ISR muß auch bereitgestellt worden sein

}

//in interrupts.c aufgerufen
void RTC_Alarm_IRQHandler(void)
{
    if(RTC_GetITStatus(RTC_IT_ALRA) != RESET)
    {
        RTC_ClearITPendingBit(RTC_IT_ALRA);
        EXTI_ClearITPendingBit(EXTI_Line17);
        reset_alarm_25s();
        print_time();
        green_LED_Toggle;
    }
}