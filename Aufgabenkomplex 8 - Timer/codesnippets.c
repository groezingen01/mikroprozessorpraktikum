//**A08-01.1**//
//in aufgabe.c, aufgerufen in main.c vor Schelife
void timer_int(){
    // Konfiguration der Interruptcontrollers
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM6_DAC_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    //Timer initialisierung
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    // TIM6 im Strukt konfigurieren
    TIM_TimeBaseStructure.TIM_Prescaler = 8400 - 1;// = 1 Takt alle 10^(-4)s = 0,1ms
    TIM_TimeBaseStructure.TIM_Period = 10000 - 1;//10^(4) * 10^(-4)s = 1s
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    // TIM6 Register aus dem Strukt Schreiben
    TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
    // TIM6 Interrupt erlauben
    TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
    // TIM 6 freigeben
    TIM_Cmd(TIM6, ENABLE);
    // Bei Start auf 0 setzen
    TIM_SetCounter(TIM6, 0);
}
//in interrupts.c
unsigned long tim6_ctr = 0;//Die zu inkrementierende Variable
void TIM6_DAC_IRQHandler()
{
    if (TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET) {
                TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
                LED_GR_TOGGLE;
                
                //USART Ausgabe
                tim6_ctr++;
                print_time();
                char tim6_txt[10];
                sprintf(tim6_txt,"%d",tim6_ctr);
                usart_2_print(tim6_txt);
    }
}
//**A08-01.2**//
//in interrupts.c
int is_meassuring = 0;
long start_time = 0;
void EXTI9_5_IRQHandler(void)
{
    //===== Taster2
    if (EXTI_GetITStatus(EXTI_Line5) == SET)
    {
        EXTI_ClearFlag(EXTI_Line5);
        EXTI_ClearITPendingBit(EXTI_Line5);
        //TASTER2_IRQ();    // ISR fuer Taste 2
        //taster2_irq();
        if (is_meassuring){
            long dauer = tim6_ctr-start_time;
            is_meassuring = 0;
            char ausgabe[128];
            sprintf(ausgabe,"Die Messung hat %d Sekunden gedauert.",dauer);
            usart_2_print(ausgabe);
            print_time();
        }else{
            usart_2_print("Noch kein Timer gestartet.");
        }
    }
	//===== nicht belegt
	if (EXTI_GetITStatus(EXTI_Line6) == SET)
	{
		EXTI_ClearFlag(EXTI_Line6);
		EXTI_ClearITPendingBit(EXTI_Line6);
		// nicht belegt
	}
	//===== nicht belegt
	if (EXTI_GetITStatus(EXTI_Line7) == SET)
	{
		EXTI_ClearFlag(EXTI_Line7);
		EXTI_ClearITPendingBit(EXTI_Line7);
		// nicht belegt
	}
	
    
    //===== Taster 1
    if (EXTI_GetITStatus(EXTI_Line8) == SET)
    {
        EXTI_ClearFlag(EXTI_Line8);
        EXTI_ClearITPendingBit(EXTI_Line8);
        if (!is_meassuring){
            start_time = tim6_ctr;
            is_meassuring = 1;
            usart_2_print("Zeitmessung startet.");
            print_time();
        }else{
            usart_2_print("Zeitmessung läuft bereits.");
        }
    }
	//===== nicht belegt
	if (EXTI_GetITStatus(EXTI_Line9) == SET)
	{
		EXTI_ClearFlag(EXTI_Line9);
		EXTI_ClearITPendingBit(EXTI_Line9);
		// nicht belegt
	}
}
//**A08-01.3**//
//in aufgabe.c, aufgerufen in main.c vor Schleife
void taster_timer(){

    //==GPIO Configuration
    // Taktsystem für den Port C Freigeben
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_StructInit(&GPIO_InitStructure);
    // Portleitung in der Struktur Konfigurieren
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    // Alternativfunktion der Portleitung Freigeben
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM3);

    //==Konfiguration der Interruptcontrollers
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    //==Timer Configuration
    // Taktsystem für Timer TIM3 Freigeben
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    // Erkennen steigender Flanke an TI1
     TIM3 -> CCMR1 |= TIM_CCMR1_CC1S_0 ;//Channel als Eingabe konfigurieren
     TIM3 -> CR2 |= TIM_CR2_TI1S ; //Input filter verbunden mit XOR aus Eingabepins Mx_CH1 bis TIMx_CH3.
    // Polarität
    TIM3 -> CCER |= TIM_CCER_CC1P ;
    // Slave Mode, external Clock Mode1, TI1FP1 Signalquelle
    TIM3 -> SMCR |= TIM_SMCR_SMS + TIM_SMCR_TS_2 + TIM_SMCR_ETF_0;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Prescaler = 1;
    TIM_TimeBaseStructure.TIM_Period = 10 - 1; // Grenzwert Tastenbetätigungen
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    // TIM3 Register aus der Struktur Schreiben
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    // TIM3 Interrupt erlauben
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

    // Counter auf 0 setzen
    TIM_SetCounter (TIM3, 0x00);

    // Timer TIM3 Freigeben
    TIM_Cmd(TIM3, ENABLE);
}
//in interrupts.c 
void TIM3_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM3, TIM_IT_Update) == SET) {
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        usart_2_print("Erreicht.");
    }
}
//in main.c als Schleife
while(1){
	wait_mSek(2000);
	int ctr = TIM_GetCounter(TIM3);
	char ausgabe[20];
	sprintf(ausgabe,"%d",ctr);
	usart_2_print(ausgabe);
}
//**A08-01.4**//
//in interrupts.c 
void TIM6_DAC_IRQHandler()
{
    if (TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET) {
                TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
                tim6_ctr++;
    }
}
//in aufgabe.c, aufgerufen via Interrupt
void reaction_test(){
    int i = 0;
    usart_2_print("start reaction test.");


    float sum=0;
    float min=-1;//-1 zum Testen ob bereits ein tatsächlicher Wert gespeichert wird
    float max=0;

    for (i=0;i<10;i++){
        int x = abs(rand_num());//Zufallszahl
        x = x%9 + 2;//verschiebt x in den Bereicht zwischen 2 und 10
        int time_to_trigger = tim6_ctr+x;
        while(tim6_ctr<time_to_trigger){;}//Warten bis zum aktivieren der LED
        green_LED_ON;
        while(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8)==1 && GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5)==0){;}//Warten auf drücken eines Tasters
        float new_time=(float)tim6_ctr-time_to_trigger+(float)TIM_GetCounter(TIM6)/10000.0;//berechnen der vergangenen Zeit
        green_LED_OFF;
        if (new_time < min || min == -1){
            min = new_time;
        }
        if (new_time > max){
            max = new_time;
        }
        sum+=new_time;
        char ausgabe[50]={0};
        sprintf(ausgabe,"new Time: %.3f",new_time);
        usart_2_print(ausgabe);
    }
    char ausgabe[50]={0};
    sprintf(ausgabe,"Min Time: %.3f",min);
    usart_2_print(ausgabe);
    sprintf(ausgabe,"Max Time: %.3f",max);
    usart_2_print(ausgabe);
    sprintf(ausgabe,"Avg. Time: %.3f",sum/10);
    usart_2_print(ausgabe);
}
//in aufgabe.c
uint32_t rand_num(void)//Aus Zusatzmaterial
{
    uint32_t zahl=0;

    RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
    RNG_Cmd(ENABLE);

    while(RNG_GetFlagStatus(RNG_FLAG_DRDY)== RESET);
    zahl = RNG_GetRandomNumber();

    RNG_Cmd(DISABLE);
    RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, DISABLE);

    return(zahl);
}
//in aufgabe.c, aufgerufen in main.c vor Schleife
void init_usart_2_irq_rx() {
    init_usart_2();
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    // Die Freigabe des zugehörigen Interrupts sieht wie folgt aus:
    USART_ClearITPendingBit(USART2, USART_IT_RXNE);
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}
//in interrupts.c
void USART2_IRQHandler(void)
{
    char zeichen;
    zeichen = (char)USART_ReceiveData(USART2);
    if (zeichen == 's'){
        reaction_test();
    }
}