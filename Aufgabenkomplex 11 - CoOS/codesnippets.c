//Aufgabe 11 - CoOS
//**A11-01.1**//
//main.c:
int main ( void )
{
    SystemInit ();  // Clocksystem initialisieren
    InitSysTick (); // Systick aktivieren
    start_RTC ();   // RTC freigeben
 
    CoInitOS ();    // CoOS Initialisierung
    Start_Task();   // Initialen Task erstellen
    CoStartOS ();   // Starten des CoOS
 
    while (1) {;}
}
//in aufgabe.c
OS_STK stack_task_1 [128];
OS_STK stack_task_2 [128];
void Start_Task(void)
{
    CoCreateTask (task_1 , 0, 0, & stack_task_1 [128 -1] , 128) ;
    CoCreateTask (task_2 , 0, 0, & stack_task_2[128 -1] , 128) ;
}
void task_1 ( void * pdata )//LED
{
    // Initialisierung der LED Portleitung
    init_leds();
    while (1)
        {
            CoTimeDelay (0,0,0,500) ;//Warte 500ms
            green_LED_Toggle;
        }
}
void task_2 ( void * pdata )//USART
{
     char zeichen = '*';

     init_usart_2_tx();
     while (1)
         {
             CoTimeDelay (0,0,2,0) ;//Warte 2s
             USART_SendData(USART2,zeichen);
         }
}
//in aufgabe.c
void init_leds(void){
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_OType =  GPIO_OType_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    // Schaltet LED an
    GPIO_SetBits(GPIOB, GPIO_Pin_2);
}
//aufgabe.c
void init_usart_2_tx(){ //initialisiert verschiecken von Daten über USART
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    // Taktsystem für die USART2 freigeben
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);


    // GPIO Port A Taktsystem freigeben
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    // USART2 TX an PA2 mit Alternativfunktion Konfigurieren
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // USART2 TX mit PA2 verbinden
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);

    // Datenprotokoll der USART einstellen
    USART_InitStructure.USART_BaudRate = 921600;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx;
    USART_Init(USART2, &USART_InitStructure);

    // USART2 freigeben
    USART_Cmd(USART2, ENABLE); // enable USART2
}
//** Der zweite Teil von A11-01.1
void task_1 ( void * pdata )//LED
{
    init_leds();
    int ctr=0;
    while (ctr<20)
    {
        CoTimeDelay (0,0,0,500) ;
        green_LED_Toggle;
        ctr++;
    }
    CoCreateTask (task_2 , 0, 0, & stack_task_2[128 -1] , 128) ;//starte Task2
    CoExitTask ();//Beende Task
}

void task_2 ( void * pdata )//USART
{
     char zeichen = '*';

     init_usart_2_tx();
     int ctr = 0;
     while (ctr<10)
     {
         CoTimeDelay (0,0,2,0) ;
         USART_SendData(USART2,zeichen);
         ctr++;
     }
     CoCreateTask (task_1 , 0, 0, & stack_task_1[128 -1] , 128) ;//starte Task1
     CoExitTask ();//Beende Task
}
void Start_Task(void)
{
    CoCreateTask (task_1 , 0, 0, & stack_task_1 [128 -1] , 128) ;
}
//A11-01
//in aufgabe.c 
OS_EventID mailbox;
OS_STK stack_task_3 [128];
OS_STK stack_task_init_uart [128];
void task_init_uart ( void * pdata )
{
    init_usart_2_irq_rx();//Initialisiert USART2 interruptfähig zum Senden UND Empfangen
    
    //Danach wird zu task_3 gewechselt
    CoCreateTask(task_3 , 0, 0, & stack_task_2[128 -1] , 128);/
    CoExitTask ();//Beende Task
}

char uart_tx_buffer[100] = {0};//Puffer für Sendedaten
void task_3(void * pdata){
    StatusType result;
    void * msg;

    mailbox = CoCreateMbox ( EVENT_SORT_TYPE_FIFO );

    for (;;)
    {
        msg = CoPendMail ( mailbox , 0 , & result );//Warte in Endlosschleife auf mail
        if ( result == E_OK)
        {
            int len=strlen(msg);
            sprintf(uart_tx_buffer,"Empfange: \"%s\" Länge: %d ", msg, len);
            usart_2_print(uart_tx_buffer);
        }
        else
        {
            usart_2_print ( "Fehler: CoPendMail nicht errfolgreich!" );
        }
    }
    CoExitTask ();
}
void Start_Task(void)
{
    CoCreateTask (task_init_uart , 0, 0, & stack_task_init_uart [128 -1] , 128) ;
}
//in aufgabe.c
void init_usart_2_irq_rx() {
    init_usart_2();//Initialisert USART2 in Empfangs- und Sendemodus.
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    // Die Freigabe des zugehörigen Interrupts
    USART_ClearITPendingBit(USART2, USART_IT_RXNE);
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}
//in interrupts.c
char uart_rx_buffer[100] = {0};
void USART2_IRQHandler(void)
{
    StatusType result;
    char zeichen;
    static int pos = 0;

    CoEnterISR ();
    // RxD - Empfangsinterrupt
    if ( USART_GetITStatus ( USART2 , USART_IT_RXNE ) != RESET)
    {
        USART_ClearITPendingBit ( USART2 , USART_IT_RXNE );//entferne flag
        zeichen = ( char ) USART_ReceiveData ( USART2 );//lese Zeichen ein
        if ( pos >= 49 )//Falls String länger als Buffer wird
        {
            memset(uart_rx_buffer, 0x00, sizeof(uart_rx_buffer));//leere Buffer
            pos = 0;
            usart_2_print( "Error:  Zeichenkette zu lang!" );
        }
        switch ( zeichen )
        {
            case '\r'://Versuche String zu Senden, wenn \r eingegeben wird
                pos = 0;
                result = isr_PostMail ( mailbox , & uart_rx_buffer );//Sende aktuellen String per Mail an Task 3
                if ( result != E_OK )//Fehlerbehandlung
                {
                    if ( result == E_SEV_REQ_FULL )
                    {
                        usart_2_print( "Error:  E_SEV_REQ_FULL!" );
                    }
                }
                break;

            default://Sonst, füge char an Sting an
                uart_rx_buffer [ pos ] = zeichen;
                pos ++;
                break;
        }
    }

    CoExitISR ();
}
//**A11-01.3**//
//in aufgabe.c
void Start_Task(void)
{
    CoCreateTask (task_1 , 0, 0, & stack_task_1 [128 -1] , 128) ;
    //CoCreateTask (task_2 , 0, 0, & stack_task_2[128 -1] , 128) ;
}
OS_FlagID flags;
OS_FlagID flag_1;
OS_FlagID flag_2;
StatusType result;
OS_STK stack_task_1 [128];
void task_1(void * pdata){
    //Benötigte Initialisierungen
    init_taste_1_irq();//Initialisiert Taster 1 interruptfähig
    init_taste_2_irq();//Initialisiert Taster 2 interruptfähig
    init_usart_2_irq_rx();//Initialisier USART 2 interruptfähig RX/TX
    init_leds();//dadurch sehen wir, dass Controller angeschaltet ist

    //Erstellen der Flags
    flag_1 = CoCreateFlag ( 1 , 0 );//1=automatischer Reset, 0= Startwert
    flag_2 = CoCreateFlag ( 1 , 0 );
    int result_flag = 0;

    for (;;)//Endlosschleife
    {
        flags = (1 << flag_1 | 1 << flag_2);//Zuweiseung der beiden Flags in die Gesamtflagge

        //prüfen ob bis mind. eine Flag in flags gesetzt, möglichen Fehler in result schreiben
        result_flag = CoAcceptMultipleFlags  ( flags , OPT_WAIT_ANY , & result );//Gibt zurück welche Flags zum Beenden des Wartens geführt haben


        if ( result == E_OK || result_flag==0 )//Kein Fehler
        {
            if ( (result_flag>>flag_1 & 1) )
            {
                CoCreateTask ( task_2 , NULL , 0 , & stack_task_2[ 128 - 1 ], 128 );//task, argv, Priorität, stack, stack größe
            }
            if ( (result_flag>>flag_2 & 1) )
            {
                CoCreateTask ( task_3, NULL , 0 , & stack_task_3 [ 128 - 1 ], 128 );
            }
            CoTickDelay ( 10 );
        }
        else
        {
            usart_2_print("Fehler beim Auslesen der Flags.");
        }
    }
    CoExitTask ();

}
OS_STK stack_task_2 [128];
void task_2(void * pdata){
    int i=0;
    for(i=0;i<5;i++){
        usart_2_print("Task Taste1 läuft jetzt");
        CoTickDelay(400);// 2s/5 = 0,4s = 400ms
    }
    CoExitTask();
}
OS_STK stack_task_3 [128];
void task_3(void * pdata){
    int i=0;
    for(i=0;i<20;i++){
        usart_2_print("Task Taste2 läuft jetzt");
        CoTickDelay(25);// 0,5s/20 = 0,025s = 25ms
    }
    CoExitTask();
}
//in aufgabe.c
void init_taste_1_irq() {

    //I/O Port Konfiguration
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_OType =  GPIO_OType_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    // Schaltet Taktsystem zu
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    //Interrupt Konfiguration
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);
    EXTI_InitTypeDef EXTI_InitStructure;
    // EXTI_Line zweisen
    EXTI_InitStructure.EXTI_Line = EXTI_Line8;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    //Interruptcontroller/NCIC Konfiguration
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_Init(&NVIC_InitStructure);
}
void init_taste_2_irq() {

    //I/O Port Konfiguration
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_OType =  GPIO_OType_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    // Schaltet Taktsystem zu
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    //Interrupt Konfiguration
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource5);
    EXTI_InitTypeDef EXTI_InitStructure;
    // EXTI_Line zweisen
    EXTI_InitStructure.EXTI_Line = EXTI_Line5;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Raising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    //Interruptcontroller/NCIC Konfiguration
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_Init(&NVIC_InitStructure);
}
//in interrupts.c
void EXTI9_5_IRQHandler(void)
{
    //===== Taster2
    if (EXTI_GetITStatus(EXTI_Line5) == SET)
    {
    EXTI_ClearFlag(EXTI_Line5);
    EXTI_ClearITPendingBit(EXTI_Line5);
    CoEnterISR();//erlaubt setzen der Flag
    int result = isr_SetFlag(flag_2);//Setzen der Flag
    CoExitISR();
    if (result != E_OK){//Wenn Fehler
        usart_2_print("Fehler beim setzen der Flag");
    }
    
    if (EXTI_GetITStatus(EXTI_Line8) == SET)
    {
    EXTI_ClearFlag(EXTI_Line8);
    EXTI_ClearITPendingBit(EXTI_Line8);
    CoEnterISR();//erlaubt setzen der Flag
    int result = isr_SetFlag(flag_1);//Setzen der Flag
    CoExitISR();
    if (result != E_OK){//Wenn Fehler
        usart_2_print("Fehler beim setzen der Flag");
    }
}
//Modifikation, so dass beide Flags gleichzeiting, in aufgabe.c
void task_1(void * pdata){

	//Benötigte Initialisierungen
    init_taste_1_irq();//Initialisiert Taster 1 interruptfähig
    init_taste_2_irq();//Initialisiert Taster 2 interruptfähig
    init_usart_2_irq_rx();//Initialisier USART 2 interruptfähig RX/TX
    init_leds();//dadurch sehen wir, dass Controller angeschaltet ist
	//Erstellen der Flags
	
    flag_1 = CoCreateFlag ( 1 , 0 );//1=automatischer Reset, 0= Startwert
    flag_2 = CoCreateFlag ( 1 , 0 );
	
    int result_flag = 0;

    for (;;)
    {
        flags = (1 << flag_1 | 1 << flag_2);//Zuweiseung der beiden Flags in die Gesamtflagge

        //prüfen ob bis mind. eine Flag in flags gesetzt, möglichen Fehler in result schreiben
        result_flag = CoAcceptMultipleFlags  ( flags , OPT_WAIT_ANY , & result );//Gibt zurück welche Flags zum Beenden des Wartens geführt haben
        result_flag = (result_flag | 1<< flag_1) | (1<<flag_2);//Setze beide Flags manuell in der result_flag

        if ( 1)//Führen immer die tasks aus
        {
            if ( (result_flag>>flag_1 & 1) )
            {
                CoCreateTask ( task_2 , NULL , 0 , & stack_task_2[ 128 - 1 ], 128 );//task, argv, Priorität, stack, stack größe
            }
            if ( (result_flag>>flag_2 & 1) )
            {
                CoCreateTask ( task_3, NULL , 0 , & stack_task_3 [ 128 - 1 ], 128 );
            }
            CoTickDelay ( 1000 );
        }
		else
        {
            usart_2_print("Fehler beim Auslesen der Flags.");
        }
	}
	CoExitTask();
}