//A02-01.1
void init_PC09(){
    GPIO_InitTypeDef GPIO_InitStructure;//Declare GPIO Configuration Struct
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);//Activate peripheral clock for GPIO C
    GPIO_StructInit(&GPIO_InitStructure);// Initialize GPIO Configuration
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;//Pin 9 is supposed to output the clock signal
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//Configure Pin 9 in alternate function mode to use clock out capability
    
    // Select push-pull with no pull
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//set speed to 50 Mhz, will be ignored
    GPIO_Init(GPIOC, &GPIO_InitStructure);//Applay the init struct to the C port
    GPIO_PinAFConfig(GPIOC,GPIO_Pin_9,GPIO_AF_MCO);//Enable the alternate function mode MCO on Pin PC9
    RCC_MCO2Config(RCC_MCO2Source_SYSCLK,RCC_MCO2Div_1);//Select SYSCLK to be outputted without dividor
}
//Aufgabe A02-02.1
if (stc_led >=500){
    if (stc_led >=3500){
        stc_led = 0;
    }else{
        LED_GR_OFF;
    }
}else{
    LED_GR_ON;
}

//Aufgabe A02-02.2
static unsigned long timer_counter = 0;
timer_counter++;
if (timer_counter >=100){
	if (timer >= 1){
		timer--;
	}
	timer_counter = 0;
}