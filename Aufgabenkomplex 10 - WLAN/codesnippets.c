//**Aufgabe 10-2.1
//in aufgabe.c, aufgerufen in main.c vor Schleife
void list_networks(){
    int i = 0;                              // Laufvariable
    char out[30]={0};                       // Ausgabe puffer
    int valid = 0;                          // Anzahl gefundener WLAN AP
    Sl_WlanNetworkEntry_t netEntries[20];   // Liste für gefundene WLAN AP
    int RetVal = -1;                        // Error Code
    _u32 interval = 1;                      // 1 Sekunden Scannen

    // WLAN Tranceiver starten
    RetVal = sl_Start(NULL, NULL, NULL);

    // WLAN Scan einschalten und scan interval festlegen
    RetVal = sl_WlanPolicySet(SL_POLICY_SCAN , 1, (_u8 *)&interval, sizeof(interval));

    usart_2_print("Suche nach Netzwerken");
    // Netzwerksliste ermitteln
    int last_valid=-1;
    timer = 50;//5s max Scandauer, implementier in Aufgabenkomplex 2
    while(valid != last_valid && timer){
        last_valid=valid;
        wait_mSek(1100);
        valid = sl_WlanGetNetworkList(0, 20, &netEntries[0]);

    }

    // WLAN Transceiver stoppen
    sl_Stop(100);

    // Netzwerksliste ausgeben
    for (i=0; i< valid; i++)
    {
        sprintf(out,"%i.",i);
        usart_2_print(out);//laufende Nummer
        print_network(netEntries[i]);
    }
}
//in aufgabe.c
void print_network(Sl_WlanNetworkEntry_t netEntrie){
    char enc_types[4][30]={
            "Unverschlüsselt",
            "WEP",
            "WPA",
            "WPA2"
    };

    char out[128];
    sprintf(out,"SSID: %s",netEntrie.ssid);
    usart_2_print(out);
    sprintf(out,"RSSI: %i",netEntrie.rssi);
    usart_2_print(out);
    sprintf(out,"VERSCHLÜSSELUNG: %s",enc_types[netEntrie.sec_type]);
    usart_2_print(out);
}